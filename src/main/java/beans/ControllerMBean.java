package beans;

import com.sun.faces.action.RequestMapping;
import dao.AlunoDAO;
import dao.ConsultaLiderDao;
import dao.EquipeDAO;
import dao.ExcecoesDAOException;
import dao.LiderDao;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import dao.LoginDAO;
import dao.UsuarioDAO;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import model.Aluno;
import model.Usuario;
import util.SessionUtils;

@ManagedBean
@SessionScoped
public class ControllerMBean implements Serializable {

    private static final long serialVersionUID = 1094801825228386363L;
    private int user, matricula;
    private String pwd;
    private String equipe;
    // modificando o html
    private boolean tipo_usuario;
    private boolean esconde_btn_equipe;
    private String troca_btn_lider;
    private String redireciona_para_equipe;
    private String messagem_erro_conta;
    private String nome_equipe;

    //acesso a banco
    private UsuarioDAO usuarioDao;
    private AlunoDAO alunoDAO;
    private EquipeDAO equipeDAO;
    private LiderDao liderDao;
    private ConsultaLiderDao consulta;
    //modelo
    private Usuario usuario;
    private Aluno aluno;

    //listas
    private List<Aluno> alunos;
    private List<Usuario> todosLideres;
    private List<Aluno> todosAlunoEquipe;

    public ControllerMBean() {

    }

    public String inicio() {
        return "admin";
    }

    public String lideres() throws ExcecoesDAOException {

        return "lideres";
    }

    public void equipes(String nome_lider) throws ExcecoesDAOException {
        equipeDAO = new EquipeDAO();
        todosAlunoEquipe = new ArrayList<>();
        todosAlunoEquipe = equipeDAO.todosAlunosEquipe(nome_lider);
    }
    //validate login

    public String validarUsuario() throws ExcecoesDAOException, InterruptedException {

        boolean valid = LoginDAO.validate(user, pwd);
        HttpSession session = SessionUtils.getSession();
        session.setAttribute("usuario", user);

        if (valid) {
            usuarioDao = new UsuarioDAO();
            usuario = new Usuario();
            usuario = usuarioDao.buscarUsuario(user);
            alunoDAO = new AlunoDAO();
            alunos = new ArrayList<>();
            alunos = alunoDAO.todosAluno();

            if (usuario.getTipo_usuario().equals("adm")) {
                tipo_usuario = true;
                esconde_btn_equipe = false;
                todosLideres = new ArrayList<>();
                troca_btn_lider = "Lideres";
                redireciona_para_equipe = "./lideres.xhtml";
                nome_equipe = "";
                todosLideres = usuarioDao.todosAlunoLideres();
                return "admin";
            } else if (usuario.getTipo_usuario().equals("lider")) {
                esconde_btn_equipe = true;
                tipo_usuario = false;
                troca_btn_lider = "Equipes";
                redireciona_para_equipe = "./equipe.xhtml";
                nome_equipe = "Nome da equipe";
                equipes(usuario.getNome_usuario());
                return "admin";
            }
            return "index";

        } else {
            //messagem_erro_conta = ;
            //  PrintWriter out = response.getWriter();
            return "index";
        }

    }

    //logout event, invalidate session
    public String logout() {
        HttpSession session = SessionUtils.getSession();
        session.invalidate();

        return "index";
    }

    public void lider(Integer matricula) throws InterruptedException, ExcecoesDAOException {

        usuarioDao = new UsuarioDAO();
        alunoDAO = new AlunoDAO();
        liderDao = new LiderDao();
        usuario = new Usuario();
        consulta = new ConsultaLiderDao();
        new Thread().sleep(2000);

        usuario = usuarioDao.buscarUsuario(matricula);
        aluno = alunoDAO.buscarAluno(matricula);
        System.out.println(usuario.getTipo_usuario());
        liderDao.LiderMod(usuario, aluno);
        System.out.println(matricula);
        System.out.println(usuario.getTipo_usuario());
        alunos = new ArrayList<>();
        validarUsuario();
    }

    /**
     *
     * @param matricula
     * @throws ExcecoesDAOException
     */
    public void equipeAdicionar(Integer matricula) throws InterruptedException, ExcecoesDAOException {
        alunoDAO = new AlunoDAO();
        aluno = new Aluno();
        equipeDAO = new EquipeDAO();
        new Thread().sleep(2000);
        aluno = alunoDAO.buscarAluno(matricula);
        System.out.println("aluno"+aluno.getNome_aluno()+ "equipe lider " + aluno.getNome_lider());
        if (aluno.getNome_lider().equals("Sem lider")) {
            usuarioDao = new UsuarioDAO();
            usuario = new Usuario();
            usuario = usuarioDao.buscarUsuario(user);
            System.out.println("ok buscado aluno " + usuario.getNome_usuario());
            equipeDAO.EquipeAdd(aluno, usuario);
           validarUsuario();
        } else {
            System.out.println("Ja possui um lider");
        }
     
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public List<Aluno> getAlunos() {
        return alunos;
    }

    public void setAlunos(List<Aluno> alunos) {
        this.alunos = alunos;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }

    public boolean isTipo_usuario() {
        return tipo_usuario;
    }

    public void setTipo_usuario(boolean tipo_usuario) {
        this.tipo_usuario = tipo_usuario;
    }

    public String getEquipe() {
        return equipe;
    }

    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }

    public String getMessagem_erro_conta() {
        return messagem_erro_conta;
    }

    public void setMessagem_erro_conta(String messagem_erro_conta) {
        this.messagem_erro_conta = messagem_erro_conta;
    }

    public List<Usuario> getTodosLideres() {
        return todosLideres;
    }

    public void setTodosLideres(List<Usuario> todosLideres) {
        this.todosLideres = todosLideres;
    }

    public String getTroca_btn_lider() {
        return troca_btn_lider;
    }

    public void setTroca_btn_lider(String troca_btn_lider) {
        this.troca_btn_lider = troca_btn_lider;
    }

    public String getRedireciona_para_equipe() {
        return redireciona_para_equipe;
    }

    public void setRedireciona_para_equipe(String redireciona_para_equipe) {
        this.redireciona_para_equipe = redireciona_para_equipe;
    }

    public boolean isEsconde_btn_equipe() {
        return esconde_btn_equipe;
    }

    public void setEsconde_btn_equipe(boolean esconde_btn_equipe) {
        this.esconde_btn_equipe = esconde_btn_equipe;
    }

    public String getNome_equipe() {
        return nome_equipe;
    }

    public void setNome_equipe(String nome_equipe) {
        this.nome_equipe = nome_equipe;
    }

    public List<Aluno> getTodosAlunoEquipe() {
        return todosAlunoEquipe;
    }

    public void setTodosAlunoEquipe(List<Aluno> todosAlunoEquipe) {
        this.todosAlunoEquipe = todosAlunoEquipe;
    }

}
