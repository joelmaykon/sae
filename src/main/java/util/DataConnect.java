package util;

import dao.ExcecoesDAOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DataConnect {

	public static Connection getConnection() {
		try {
			 Class.forName("org.postgresql.Driver");
            return DriverManager.getConnection("jdbc:postgresql://localhost:5432/bd_sistema_web_sae", 
                    "postgres", "root");
		} catch (Exception ex) {
			System.out.println("Database.getConnection() Error -->"
					+ ex.getMessage());
			return null;
		}
	}

	public static void closeConnection(Connection conn, Statement stmt, ResultSet rs) throws ExcecoesDAOException{
        close(conn, stmt, rs);
    }
    public static void closeConnection(Connection conn, Statement stmt) throws ExcecoesDAOException{
            close(conn, stmt, null);
    }
    
    public static void closeConnection(Connection conn) throws ExcecoesDAOException{
            close(conn, null, null);
    } 
    public static void close (Connection conn, Statement stmt, ResultSet rs) throws ExcecoesDAOException{
        try{
            if(rs != null) rs.close();
            if(stmt != null) stmt.close();
            if(conn != null) conn.close();
        }catch(Exception e){
            throw new ExcecoesDAOException(e.getMessage());
        }
    }
}