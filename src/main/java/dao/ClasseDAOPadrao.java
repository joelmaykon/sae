/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author joel
 */
public class ClasseDAOPadrao {
    /*
    Connection conn = null;
    public void atualizarNome(Teste teste) throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        if (teste == null) {
            throw new ExcecoesDAOException("O valor passado não pode ser nulo.");
        }

        try {
            String SQL = "UPDATE dados_sae.Evento SET nome_evento=?" + "WHERE nome_evento=?' AND ano=?;";
            conn = this.conn;
            ps = conn.prepareStatement(SQL);
            ps.setString(1, evento.getNome());
            ps.setString(2, evento.getNome());
            ps.setInt(3, evento.getAno());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException("Erro ao atualizar dados:" + sqle);
        } finally {
            DataConnect.closeConnection(conn, ps);
        }
    }

    public void excluir(Evento evento) throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        if (evento == null) {
            throw new ExcecoesDAOException("O valor passado não pode ser nulo.");
        }

        try {
            conn = this.conn;
            ps = conn.prepareStatement("DELETE FROM Evento WHERE nome=? AND ano=?");
            ps.setString(1, evento.getNome());
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException("Erro ao excluir dados:" + sqle);
        } finally {
            DataConnect.closeConnection(conn, ps);
        }
    }

    public void salvar(Evento evento) throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        if (evento == null) {
            throw new ExcecoesDAOException("O valor passado nao pode ser nulo.");
        }
        try {
            String SQL = "INSERT INTO Eventos(codEvento,nomeEvento,coordenadorEvento)" + "VALUES(?,?,?)";
            conn = this.conn;
            ps = conn.prepareStatement(SQL);
            ps.setString(1, evento.getNome());
            ps.execute();
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException("Erro ao inserir dados" + sqle);

        } finally {
            DataConnect.closeConnection(conn, ps);
        }
    }

    public List todosEventos() throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = this.conn;
            ps = conn.prepareStatement("SELECT * FROM Evento");
            rs = ps.executeQuery();
            List<Evento> list = new ArrayList<>();
            while (rs.next()) {
                String nome = rs.getString(1);
                String local = rs.getString(2);
                String descricao = rs.getString(3);
                String responsavel = rs.getString(4);
                int ano = rs.getInt(5);

                list.add(new Evento(nome, local, descricao, responsavel, ano));
            }
            return list;
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException(sqle);
        } finally {
            DataConnect.closeConnection(conn, ps, rs);
        }
    }

    public Evento procurarEventoNome(String nome_evento, String ano_evento) throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = this.conn;
            ps = conn.prepareStatement("SELECT * FROM Eventos" + "WHERE nome_evento=?' AND ano=?;");

            ps.setString(1, nome_evento);
            ps.setString(2, ano_evento);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new ExcecoesDAOException("Não foi encontrado nenhum registro com esse nome ou data");
            }

            String nome = rs.getString(1);
            String descricao = rs.getString(2);
            String local = rs.getString(3);
            int ano = rs.getInt(4);
            String responsavel = rs.getString(5);

            return new Evento(nome, local, descricao, responsavel, ano );
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException(sqle);
        } finally {
            DataConnect.close(conn, ps, rs);
        }
    }
     */
}
