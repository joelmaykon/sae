package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Usuario;

import util.DataConnect;

public class LoginDAO {
  
 
  public static boolean validate(int usuario, String senha) throws ExcecoesDAOException {
    Connection con = null;
    PreparedStatement ps = null;

    try {
      con = DataConnect.getConnection();
      ps = con.prepareStatement("Select login, nome_usuario, tipo_usuario,senha "
              + "from usuario where login = ? and senha = ?;");
      ps.setInt(1, usuario);
      ps.setString(2, senha);
      ResultSet rs = ps.executeQuery();

      if (rs.next()) {
        System.out.println("Deu Certo entramos na conta "+ usuario);
        return true;
        
      }
    } catch (SQLException ex) {
      System.out.println("Login error -->" + ex.getMessage());
      return false;
    } finally {
      DataConnect.closeConnection(con);
    }
    return false;
  }
 
}
