/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Aluno;
import model.Usuario;
import util.DataConnect;

/**
 *
 * @author joelmaykon
 */
public class EquipeDAO {

    private Connection conn;

    public EquipeDAO() throws ExcecoesDAOException {
        this.conn = DataConnect.getConnection();
    }

    public void EquipeAdd(Aluno aluno, Usuario usuario) throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        if (aluno == null) {
            throw new ExcecoesDAOException("O valor passado não pode ser nulo.");
        }

        try {
            //Converter um usuario aluno em um usuario lider
            String SQL = "UPDATE aluno SET nome_lider= ?" + " WHERE matricula=?";
            conn = this.conn;
            ps = conn.prepareStatement(SQL);
            ps.setString(1, usuario.getNome_usuario());
            ps.setInt(2, aluno.getMatricula());
            System.out.println("Usuario " + aluno.getNome_lider() + " alterado");
            ps.executeUpdate();
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException("Erro ao atualizar dados:" + sqle);
        } finally {
            DataConnect.closeConnection(conn, ps);
        }
    }

    public List todosAlunosEquipe(String nome_lider_atual) throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = this.conn;
            ps = conn.prepareStatement("SELECT * FROM aluno  WHERE nome_lider= ?;");
            ps.setString(1, nome_lider_atual);
            rs = ps.executeQuery();
            List<Aluno> list = new ArrayList<>();
            while (rs.next()) {
                String nome_aluno = rs.getString(1);
                String diretoria = rs.getString(2);
                String modalidade = rs.getString(3);
                String nome_lider = rs.getString(4);
                int matricula = rs.getInt(5);
                String tipo_aluno = rs.getString(6);
                list.add(new Aluno(nome_aluno, diretoria, modalidade, nome_lider, matricula, tipo_aluno));
                System.out.println("entrou na vizualização de equipes");
            }
            return list;
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException(sqle);
        } finally {
            DataConnect.closeConnection(conn, ps, rs);
        }
    }

}
