/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Usuario;
import util.DataConnect;

/**
 *
 * @author joelmaykon
 */
public class UsuarioDAO {

    public UsuarioDAO() throws ExcecoesDAOException{

        this.conn = DataConnect.getConnection();
    }

    private Connection conn;
   public Usuario buscarUsuario(int login) throws ExcecoesDAOException {
    PreparedStatement ps = null;
    Connection conn = null;
    ResultSet rs = null;
    try {
      conn = DataConnect.getConnection();
      ps = conn.prepareStatement("Select login,  nome_usuario, tipo_usuario from Usuario where login = ?;");
      ps.setInt(1, login);
      rs = ps.executeQuery();
      if (!rs.next()) {
        throw new ExcecoesDAOException("Não foi encontrado nenhum registro com esse login");
      }

      login = rs.getInt(1);
      String nome_usuario = rs.getString(2);
      String tipo_usuario= rs.getString(3);
      return new Usuario(login, nome_usuario, tipo_usuario);

    } catch (SQLException sqle) {
      throw new ExcecoesDAOException(sqle);
    } finally {
      DataConnect.close(conn, ps, rs);
    }

  }
    public List todosAlunoLideres() throws ExcecoesDAOException {
    PreparedStatement ps = null;
    Connection conn = null;
    ResultSet rs = null;
    try {
      conn = this.conn;
      ps = conn.prepareStatement("SELECT * FROM usuario WHERE tipo_usuario='lider';");
      rs = ps.executeQuery();
      List<Usuario> list = new ArrayList<>();
     
      while (rs.next()) {
      int matricula = rs.getInt(1);
      String nome_usuario = rs.getString(2);
      String tipo_usuario = rs.getString(3);
      
       list.add(new Usuario(matricula, nome_usuario, tipo_usuario));
          System.out.println("ok");
      }
      return list;
    } catch (SQLException sqle) {
      throw new ExcecoesDAOException(sqle);
    } finally {
      DataConnect.closeConnection(conn, ps, rs);
    }
  }
}
