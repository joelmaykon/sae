/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Aluno;
import model.Usuario;
import util.DataConnect;

/**
 *
 * @author joelmaykon
 */
public class AlunoDAO {

  private Connection conn;

  public AlunoDAO() throws ExcecoesDAOException {
    this.conn = DataConnect.getConnection();
  }

  
  public List todosAluno() throws ExcecoesDAOException {
    PreparedStatement ps = null;
    Connection conn = null;
    ResultSet rs = null;
    try {
      conn = this.conn;
      ps = conn.prepareStatement("SELECT * FROM aluno");
      rs = ps.executeQuery();
      List<Aluno> list = new ArrayList<>();
      while (rs.next()) {
        String nome_aluno = rs.getString(1);
      String diretoria = rs.getString(2);
      String modalidade = rs.getString(3);
      String nome_lider = rs.getString(4);
      int matricula = rs.getInt(5);
      String tipo_aluno = rs.getString(6);
       list.add(new Aluno(nome_aluno,diretoria,modalidade,nome_lider,matricula,tipo_aluno));

      }
      return list;
    } catch (SQLException sqle) {
      throw new ExcecoesDAOException(sqle);
    } finally {
      DataConnect.closeConnection(conn, ps, rs);
    }
  }
  

   

    public Aluno buscarAluno(Integer matricula) throws ExcecoesDAOException {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        try {
            conn = this.conn;
            ps = conn.prepareStatement("SELECT * FROM aluno" + " WHERE matricula=?;");

            ps.setInt(1, matricula);
            rs = ps.executeQuery();
            if (!rs.next()) {
                throw new ExcecoesDAOException("Não foi encontrado nenhum registro com esse nome ou data");
            }
            String nome_aluno = rs.getString(1);
            String diretoria = rs.getString(2);
            String modalidade = rs.getString(3);
            String nome_lider = rs.getString(4);
            int matricula_aluno = rs.getInt(5);

            return new Aluno(nome_aluno, diretoria, modalidade, nome_lider, matricula_aluno);
        } catch (SQLException sqle) {
            throw new ExcecoesDAOException(sqle);
        } finally {
            DataConnect.close(conn, ps, rs);
        }
    }
    
   
}
