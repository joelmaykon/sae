/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author joelmaykon
 */
public class Aluno {
   private int matricula;
  private String nome_aluno;
  private String diretoria;
  private String modalidade;
  private String nome_lider;
  private String tipo_aluno;
  /**
   * @return the matricula_aluno
   */
  public Aluno() {

  }

    public Aluno(String nome_aluno, String diretoria, String modalidade, String nome_lider,int matricula) {
        this.nome_aluno = nome_aluno;
        this.diretoria = diretoria;
        this.modalidade = modalidade;
        this.nome_lider = nome_lider;
        this.matricula = matricula;
    }
     public Aluno(String nome_aluno, String diretoria, String modalidade, String nome_lider,int matricula, String tipo) {
        this.nome_aluno = nome_aluno;
        this.diretoria = diretoria;
        this.modalidade = modalidade;
        this.nome_lider = nome_lider;
        this.matricula = matricula;
        this.tipo_aluno = tipo;
    }

    public String getNome_aluno() {
        return nome_aluno;
    }

    public void setNome_aluno(String nome_aluno) {
        this.nome_aluno = nome_aluno;
    }

    public String getDiretoria() {
        return diretoria;
    }

    public void setDiretoria(String diretoria) {
        this.diretoria = diretoria;
    }

    public String getModalidade() {
        return modalidade;
    }

    public void setModalidade(String modalidade) {
        this.modalidade = modalidade;
    }

    public String getNome_lider() {
        return nome_lider;
    }

    public void setNome_lider(String nome_lider) {
        this.nome_lider = nome_lider;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getTipo_aluno() {
        return tipo_aluno;
    }

    public void setTipo_aluno(String tipo_aluno) {
        this.tipo_aluno = tipo_aluno;
    }

    

}
