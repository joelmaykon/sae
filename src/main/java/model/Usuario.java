/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author joelmaykon
 */
public class Usuario {

  private int usuario;
  private String senha;
  private String nome_usuario;
  private String tipo_usuario;

  public Usuario() {
  }

  public Usuario(int usuario, String nome_usuario, String tipo_usuario) {
    this.usuario = usuario;
    this.nome_usuario = nome_usuario;
    this.tipo_usuario = tipo_usuario;
  }

  

  /**
   * @return the nome_usuario
   */
  public String getNome_usuario() {
    return nome_usuario;
  }

  /**
   * @param nome_usuario the nome_usuario to set
   */
  public void setNome_usuario(String nome_usuario) {
    this.nome_usuario = nome_usuario;
  }

  /**
   * @return the tipo_usuario
   */
  public String getTipo_usuario() {
    return tipo_usuario;
  }

  /**
   * @param tipo_usuario the tipo_usuario to set
   */
  public void setTipo_usuario(String tipo_usuario) {
    this.tipo_usuario = tipo_usuario;
  }

  /**
   * @return the usuario
   */
  public int getUsuario() {
    return usuario;
  }

  /**
   * @param usuario the usuario to set
   */
  public void setUsuario(int usuario) {
    this.usuario = usuario;
  }

  /**
   * @return the senha
   */
  public String getSenha() {
    return senha;
  }

  /**
   * @param senha the senha to set
   */
  public void setSenha(String senha) {
    this.senha = senha;
  }

}
