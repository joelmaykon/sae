/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author joelmaykon
 */
public class Equipe {

  
  private int matricula_aluno;
  private String nome_aluno;
  private String nome_equipe;
  private int qtd_participantes = 0;

  public Equipe() {
  }

  public Equipe(int matricula_aluno, String nome_aluno, String nome_equipe,
          int qtd_participantes) {
    this.matricula_aluno = matricula_aluno;
    this.nome_aluno = nome_aluno;
    this.nome_equipe = nome_equipe;
    this.qtd_participantes = qtd_participantes;
  }

  public int getMatricula_aluno() {
    return matricula_aluno;
  }

  public void setMatricula_aluno(int matricula_aluno) {
    this.matricula_aluno = matricula_aluno;
  }

  public String getNome_aluno() {
    return nome_aluno;
  }

  public void setNome_aluno(String nome_aluno) {
    this.nome_aluno = nome_aluno;
  }
 

  public String getNome_equipe() {
    return nome_equipe;
  }

  public void setNome_equipe(String nome_equipe) {
    this.nome_equipe = nome_equipe;
  }
  
  /**
   * @return the qtd_participantes
   */
  public int getQtd_participantes() {
    return qtd_participantes;
  }

  /**
   * @param qtd_participantes the qtd_participantes to set
   */
  public void setQtd_participantes(int qtd_participantes) {
    this.qtd_participantes = qtd_participantes;
  }
}
