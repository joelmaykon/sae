# Sistema-Academico-de-Esportes-IFRN
Práticas acadêmicas de desenvolvimento de software para Sistemas Web.


  *  Escopo do sistema

# SAE - SISTEMA ADMINISTRATIVO DE ESPORTES

## 1. Introdução

### 1.1 Resumo
      
  Este documento tem o objetivo descrever as funcionalidades, requisitos e restrições do Sistema Administrativo de Esportes que busca atende a necessidade de divulgação e cadastros de informações do setor de educação física do IFRN.

### 1.2 Escopo

O SAE, Sistema administrativo de Esportes visa a criação de um sistema para facilitar o gerenciamento de informações e divulgação de eventos do setor de educação física. O sistema é utilizado em um navegador web, tem a possibilidade do administrador gerenciar eventos e visualizar dados de atletas e equipes das modalidades cadastradas. Os outros usuários que não são administradores podem acessar o sistema e visualizar eventos ativos e pode se cadastrar em eventos. O sistema inicialmente ira atender a demanda do evento Esportivo Jogos Internos do IFRN, trabalhando com quatro modalidades, Futsal, Basquete, Natação e Atletismo. 
 
## -Requisitos-


### Funcionais

| Cod.| Nome | Descrição | Categoria | Prioridade |
| :----------- | :----------- | :----------- | :----------- | :----------- | :----------- |
| F01     | Administrar Eventos  | O sistema deve permitir ao administrador  eventos, editar e excluir. A principio o evento será apenas os Jogos Internos do CNAT-IFRN     | Evidente       | Alta  |
| F02       | Cadastrar administrador   | Administrador será cadastrado no sistema para poder gerenciar o sistema.      | Evidente| Baixa|
| F03       | Cadastrar Professor   | Administrador poderá cadastrar no sistema professores para utilizar o sistema.  | Evidente| Baixa|
| F04       | Cadastrar usuário   | Usuário será cadastrado no sistema para poder participar de atividades esportivas.     | Evidente| Baixa|
| F05       | Acesso a eventos   | Usuários poderão visualizar informações sobre eventos esportivos do IFRN.      | Evidente| Alta|
| F06       | Cadastrar em eventos   | O usuário pode acessar ao evento e se inscrever para ser participante nas modalidades de Futsal, Basquete, Natação e Atletismo.    | Evidente| Alta|
| F07       | Selecionar Lider   | O professor cadastrado podera selecionar um atleta como lider de equipe    | Evidente| Alta|
| F08       | Formar equipes   | O lider da equipe vai cadastrar os participantes do seu time na modalidade (Futsal e Basquete)    | Evidente| Alta|
| F09       | Consultar dados de participantes   | O administrador pode acessar ao evento e visualizar dados dos participantes.    | Evidente| Alta|
| F10       | Criar Evento  |O Administrador tem a possibilidade de criar eventos predefinidos.  A principio o evento será apenas os Jogos Internos do CNAT-IFRN    | Evidente| Alta|

### Não Funcionais

| Cod.| Nome | Descrição | Categoria | Duração |
| :----------- | :----------- | :----------- | :----------- | :----------- | :----------- |
| NF01     | Autenticação  | O sistema deve permitir apenas o administrador e os docentes gerenciar eventos.      | Obrigatório       | Permanente  |
| NF02       | Sistema Web   | O sistema precisa ser utilizado em  um navegador Web.    | Obrigatório| Permanente|
| NF03       |  Ter acesso aos eventos criados na página principal  | Todos os usuário  devem ter acesso primeiramente aos eventos ativos.  | Obrigatório| Permanente|
| NF04       |Guardar dados cadastrados no sistema  | Os dados dos participantes de eventos devem ser guardados para o administrador ter acesso a eles.     | Obrigatório| Permanente|
| NF05       | Consulta de dados cadastrados   | O sistema deve permitir acesso do administrador aos dados cadastrados em determinado evento.     | Obrigatório| Permanente|
| NF06       | Consultar equipes   | O sistema vai mostrar as equipes formadas pelas modalidades (Futsal, Basquete, Natação e Atletismo) e por diretorias    | Obrigatório| Permanente|
| NF07       |  Perfis de usuários  | O sistema deve ter a área do usuário de acordo com seu nível de acesso ao sistema  | Desejável| Permanente|
| NF08       | Dados sobre docentes da Educação Física  | Todos usuários podem visualizar os docentes e algumas informações sobre os mesmos  | Desejável| permanente|

### Relacionamento entre os requisitos

| ---| F01 | F02 | F03 | F04 |F05 |F06 |F07 | F08 |F09 | F10 |
| :----------- | :-----------: | :-----------: | :-----------: | :-----------: | :-----------: |:-----------: |:-----------: |:-----------: |:-----------: |:-----------: |
| NF01     |  X  |  X  |  X | X |    |    |   |   | X | X |
| NF02     |  X  |  X  |  X | X | X  | X  |   |   |   | X |
| NF03     |     |     |    |   | X  |  X |   |   |   | X |
| NF04     |     |     |    |   |    |    |   |   | X |   |
| NF05     |     |     |    | X |    |    | X | X | X |   |
| NF06     |     |     |    |   |    |    | X | X |   |   |
| NF07     |     |  X  |  X | X |    |    |   |   | X |   |
| NF08     |     |     |  X |   |    |    |   |   |   |   |

# Modelagem dos casos de uso

  * Caso de uso geral - Descrito brevemente, os casos de uso estão como os requisitos que o sistema possui. 
         ![Caso_de_uso_geral](/uploads/36bddad07ae4d9b37e71b6bba9db97d0/UCDGeralSae.png)

### Casos de uso:

| Cod.| Caso de uso | Descrição | Atores | Classificação |
| :----------- | :----------- | :----------- | :----------- | :----------- | :----------- | :----------- | :----------- | :----------- |
| [UC01](https://gitlab.devops.ifrn.edu.br/tads.cnat/pdsweb/2018.1/sae/sae/wikis/documenta%C3%A7%C3%A3o-do-software)     | Autenticação para Administradores, Lideres de equipe e Alunos |  A partir do login  é permitido fazer o gerenciamento do sistema      | Administrador,Lider de equipe e Aluno      | Primário |
| UC02       | Login como usuário   | A partir do login do usuário, os atletas poderão se inscrever no evento    | Usuário | Primário |
| [UC03] (https://gitlab.devops.ifrn.edu.br/tads.cnat/pdsweb/2018.1/sae/sae/wikis/caso-de-uso-03-Administrar-evento)      |  Administrar eventos  | Fazer a criação do Evento Jogos internos, edição dos dados e exclusão | Administrador | Primário |
| UC04       |  Cadastrar Administrador  | Cadastrar novos usuários com privilégios Administrador | Administrador | Secundário |
| UC05      | Cadastrar Professores   | O administrador poderá cadastrar  usuários professor que possui alguns privilégios de administrador     | Administrador | Secundário |
|[UC06](https://gitlab.devops.ifrn.edu.br/tads.cnat/pdsweb/2018.1/sae/sae/wikis/Caso-de-uso-06-criar-eventos)      | Criar eventos   | O administrador poderá cadastrar novo evento, com isso o evento ficara disponível para visualização     | Administrador | Primário |
| UC07       | Cadastrar no sistema   | O usuário poderá se cadastrar no sistema, adicionando informações pessoas   | Usuário | Primário |
| UC08       | Acessar ao evento   | O usuário poderá informações sobre o evento, como datas e locais   | Usuário | Secundário |
| [UC09](https://gitlab.devops.ifrn.edu.br/tads.cnat/pdsweb/2018.1/sae/sae/wikis/caso-de-uso-inscrever-se-em-evento)       |  Inscrever em evento  | Com o usuário logado, é permitindo fazer a inscrição no evento que esta com o prazo em aberto   | Usuário | Primário |
| UC10       | Formar equipes  | Para as modalidades em equipe, Futsal e Basquete, um usuário LÍDER ira adicionar os participantes da equipe  | Usuário | Secundário |
| UC11       | Consultar dados dos alunos  | O administrador poderá visualizar informação sobre o usuário cadastrado no sistema, bem como no evento em que esta cadastrado.  | Administrador | Secundário |
| UC12       | visualizar jogos do evento  | todos podem visualizar os jogos do evento.  | todos usuários | Secundário |


### Relacionamento entre os requisitos e Casos de uso:

| ---| F01 | F02 | F03 | F04 |F05 |F06 |F07 | F08 |F09 |NF01 |NF02 |NF03 |NF04 |NF05 |NF06 |NF07 |NF08 |
| :----------- | :-----------: | :-----------: | :-----------: | :-----------: | :-----------: |:-----------: |:-----------: |:-----------: |:-----------: |:-----------: |:-----------: |:-----------: |:-----------: |:-----------: |:-----------: |:-----------: |:-----------: |:-----------: |
| CDU01     | X   |    | X |  |      |  X |   |   | X | X |   |   |   | X | X |   |   |
| CDU02     |     |    |   |  | X    |    |   | X |   |   | X | X |   |   |   | X | X |
| CDU03     |  X  |     |    |   |    |  X |   |   |   |   |   |   |   |   | X |   |   |
| CDU04     |     |  X  |  X |   |    |  X |   |   | X |   |   |   |   | X | X |   |   |
| CDU05     |  X  |     |    |   |    |  X | X |   | X | X | X |   |   | X | X |   |   |
| CDU06     |     |  X  |  X |   |    |    |   |   |   |   |   |   | X |   |   |   |   |
| CDU07     |     |     |    | X |  X |    |   | X |   |   |   |   |   |   |   | X |   |
| CDU08     |     |     |    |   |  X |    |   | X |   |   |   |   |   |   |   |   |   |
| CDU09    |     |     |    |  X |    |    |   | X |   |   |   | X |   |   |    |   |   
| CDU10     |     |     |    |   | X  |    |   |   |   |   |   | X |   |   |   |   |   |
| CDU11     |     |     |    |   | X  |    |   |   | X |   |   | X | X | X |   | X |   |


 ### Regras de Negócios:

| Cod.| Caso de uso | Descrição | Atores | Classificação |
| :----------- | :----------- | :----------- | :----------- | :----------- | :----------- | :----------- | :----------- | :----------- |
| [UC01](https://gitlab.devops.ifrn.edu.br/tads.cnat/pdsweb/2018.1/sae/sae/wikis/documenta%C3%A7%C3%A3o-do-software)     | Autenticação para Administradores, Lideres de equipe e Alunos |  A partir do login  é permitido fazer o gerenciamento do sistema      | Administrador,Lider de equipe e Aluno      | Primário |
| UC02       | Login como usuário   | A partir do login do usuário, os atletas poderão se inscrever no evento    | Usuário | Primário |
| [UC03] (https://gitlab.devops.ifrn.edu.br/tads.cnat/pdsweb/2018.1/sae/sae/wikis/caso-de-uso-03-Administrar-evento)      |  Administrar eventos  | Fazer a criação do Evento Jogos internos, edição dos dados e exclusão | Administrador | Primário |
| UC04       |  Cadastrar Administrador  | Cadastrar novos usuários com privilégios Administrador | Administrador | Secundário |
| UC05      | Cadastrar Professores   | O administrador poderá cadastrar  usuários professor que possui alguns privilégios de administrador     | Administrador | Secundário |
|[UC06](https://gitlab.devops.ifrn.edu.br/tads.cnat/pdsweb/2018.1/sae/sae/wikis/Caso-de-uso-06-criar-eventos)      | Criar eventos   | O administrador poderá cadastrar novo evento, com isso o evento ficara disponível para visualização     | Administrador | Primário |
| UC07       | Cadastrar no sistema   | O usuário poderá se cadastrar no sistema, adicionando informações pessoas   | Usuário | Primário |
| UC08       | Acessar ao evento   | O usuário poderá informações sobre o evento, como datas e locais   | Usuário | Secundário |
| [UC09](https://gitlab.devops.ifrn.edu.br/tads.cnat/pdsweb/2018.1/sae/sae/wikis/caso-de-uso-inscrever-se-em-evento)       |  Inscrever em evento  | Com o usuário logado, é permitindo fazer a inscrição no evento que esta com o prazo em aberto   | Usuário | Primário |
| UC10       | Formar equipes  | Para as modalidades em equipe, Futsal e Basquete, um usuário LÍDER ira adicionar os participantes da equipe  | Usuário | Secundário |
| UC11       | Consultar dados dos alunos  | O administrador poderá visualizar informação sobre o usuário cadastrado no sistema, bem como no evento em que esta cadastrado.  | Administrador | Secundário |
| UC12       | visualizar jogos do evento  | todos podem visualizar os jogos do evento.  | todos usuários | Secundário |
